
echo 'installing babun tools'

pact install python python-paramiko python-crypto gcc-g++ wget openssh python-setuptools
python /usr/lib/python2.7/site-packages/easy_install.py pip
pip install ansible


echo 'Adding symlink ~/.vagrant.d/ -> %userprofile%/.vagrant.d/'
ln -fs $BABUN_HOME/../.vagrant.d ~/.vagrant.d


echo  'Create ansible_galaxy and ansible_playbook files'

echo '@echo off ' > $BABUN_HOME/ansible-galaxy.bat
echo 'set CYGWIN=%USERPROFILE%\\.babun\\cygwin ' >> $BABUN_HOME/ansible-galaxy.bat
echo 'set SH=%CYGWIN%\\bin\\zsh.exe ' >> $BABUN_HOME/ansible-galaxy.bat
echo '"%SH%" -c "/bin/ansible-galaxy %*"' >> $BABUN_HOME/ansible-galaxy.bat

echo '@echo off ' > $BABUN_HOME/ansible-playbook.bat
echo 'set CYGWIN=%USERPROFILE%\\.babun\\cygwin ' >> $BABUN_HOME/ansible-playbook.bat
echo 'set SH=%CYGWIN%\\bin\\zsh.exe ' >> $BABUN_HOME/ansible-playbook.bat
echo '"%SH%" -c "/bin/ansible-playbook %*"' >> $BABUN_HOME/ansible-playbook.bat



echo 'overwrite ~/.ansible.cfg'

echo '[ssh_connection]' > ~/.ansible.cfg
echo 'control_path = /tmp' >> ~/.ansible.cfg
